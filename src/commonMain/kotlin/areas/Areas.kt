/*
 * Copyright 2020 WildFyre
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an "AS IS"
 * BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 */

package net.wildfyre.lib.areas

import io.ktor.http.HttpMethod
import net.wildfyre.lib.WildFyre
import net.wildfyre.lib.api.ApiArea
import net.wildfyre.lib.api.ApiReputation
import net.wildfyre.lib.utils.SemaphoreCache

/**
 * Allows to access WildFyre's [areas][Area].
 *
 * Areas are a core concept: every post is local to an area.
 */
class Areas(private val wildfyre: WildFyre) {

    /**
     * Get the list of areas available to the authenticated user.
     */
    suspend fun getAll(): List<Area> =
        wildfyre.api.request<List<ApiArea>>(HttpMethod.Get, "/areas/").map { Area(wildfyre, it) }

    //TODO in T403: Better performance
    suspend fun get(id: String) = getAll().find { it.id == id }

    internal val reputations = SemaphoreCache<String, ApiReputation>(wildfyre) { id ->
        wildfyre.api.request(HttpMethod.Get, "/areas/$id/rep/")
    }
}
