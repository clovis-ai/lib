/*
 * Copyright 2020 WildFyre
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an "AS IS"
 * BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 */

package net.wildfyre.lib.utils

/**
 * Helper functions for cached objects.
 */
interface CachedObject<I, T> {

    /**
     * The ID of the object.
     */
    val id: I

    /**
     * The cache responsible for this object.
     */
    val cache: Cache<I, T>

    /**
     * Queries the cache to get a newer version of this object.
     * This method will not mutate the current object.
     *
     * @param forceUpdate Force the cache to refresh the object from the API. This is the same as calling
     * [flagAsOutdated] before this method.
     */
    suspend fun getNewVersion(forceUpdate: Boolean = false): T {
        if (forceUpdate) flagAsOutdated()

        return cache.get(id)
    }

    /**
     * Flags this object has outdated.
     *
     * Convenience utility around [Cache.flagAsOutdated].
     */
    suspend fun flagAsOutdated() = cache.flagAsOutdated(id)

}
