/*
 * Copyright 2020 WildFyre
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an "AS IS"
 * BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 */

package net.wildfyre.lib.api

import kotlinx.serialization.Serializable

@Serializable
internal data class ApiPostRecursive(
    val id: Int,
    val author: ApiUser?,
    val anonym: Boolean,
    val subscribed: Boolean,
    val created: String,
    val active: Boolean,
    val text: String,
    val image: String?,
    val additional_images: List<String>,
    val comments: List<ApiCommentRecursive>
)

internal fun ApiPostRecursive.toNonRecursive() = ApiPost(id,
    author?.user,
    anonym,
    subscribed,
    created,
    active,
    text,
    image,
    additional_images,
    comments.map { it.toNonRecursive() })

@Serializable
internal data class ApiPost(
    val id: Int,
    val author: Int?,
    val anonym: Boolean,
    val subscribed: Boolean,
    val created: String,
    val active: Boolean,
    val text: String,
    val image: String?,
    val additional_images: List<String>,
    val comments: List<ApiComment>
)

@Serializable
internal data class ApiPostsRecursive(
    val count: Int, val next: String?, val previous: String?, val results: List<ApiPostRecursive>
)
