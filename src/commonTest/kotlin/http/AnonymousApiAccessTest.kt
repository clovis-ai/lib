/*
 * Copyright 2020 WildFyre
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an "AS IS"
 * BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 */

package http

import TestUtils.runTest
import net.wildfyre.lib.WildFyre
import net.wildfyre.lib.WildFyreAnonymous
import kotlin.test.Test

class AnonymousApiAccessTest {

    @Test
    fun connect_to_local_api() = runTest {
        WildFyreAnonymous.connect(WildFyre.LOCAL_API).areas()
    }

    @Test
    fun connect_to_production_api() = runTest {
        WildFyreAnonymous.connect(WildFyre.PRODUCTION_API).areas()
    }

}
