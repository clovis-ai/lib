/*
 * Copyright 2020 WildFyre
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an "AS IS"
 * BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express
 * or implied.
 * See the License for the specific language governing permissions
 * and limitations under the License.
 */

package account

import TestUtils.runTest
import freshTestingApi
import testingApi
import kotlin.test.Test
import kotlin.test.assertEquals
import kotlin.test.assertNull
import kotlin.test.assertTrue

class AccountTest {

    @Test
    fun get_account() = runTest {
        val api = testingApi()

        with(api.account) {
            assertEquals(2, id)
            assertEquals("user", username)
            assertEquals("user@example.invalid", email)
        }
    }

    @Test
    fun refresh_account() = runTest {
        val api = freshTestingApi()

        with(api.account) {
            assertTrue { username.startsWith("new-user-") }
            assertNull(email, "The account was just created, the email shouldn't be verified; found: $email")

            refresh()
        }
    }

}
